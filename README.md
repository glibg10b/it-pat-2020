# Grade 10 IT PAT

## Features

- Mono-alphabetic simple substitution cipher
- Ability to copy text to/from clipboard
- Ability to en-/decrypt files
- A progress bar

## User interface

![Main window](res/main-window.png)
