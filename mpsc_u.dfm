object frmMpsc: TfrmMpsc
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Multi-pass mono-alphabetic substitution cipher/decipher program'
  ClientHeight = 497
  ClientWidth = 645
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblInstructions: TLabel
    Left = 8
    Top = 8
    Width = 594
    Height = 143
    Caption = 
      'To encipher text, paste the text into the first text box (labele' +
      'd "Plain text"), choose the amount of passes then click on the "' +
      'Encipher" button.'#13#10'Using more passes increases the security of t' +
      'he text, but makes enciphering and deciphering take longer.'#13#10#13#10'T' +
      'o decipher text, paste the text into the second text box (labele' +
      'd "Cipher text"), choose the amount of passes then click on the ' +
      '"Decipher" button.'#13#10'Make sure you choose the amount of passes th' +
      'at was used to encipher the text.'#13#10#13#10'Only ASCII characters are a' +
      'llowed in the edit boxes, any other characters will be ignored.'#13 +
      #10#13#10'Tip: you can use Tab and Shift+Tab to move between elements.'
    WordWrap = True
  end
  object progbarProgress: TProgressBar
    Left = 8
    Top = 462
    Width = 629
    Height = 27
    ParentShowHint = False
    Position = 100
    Smooth = True
    State = pbsPaused
    ShowHint = False
    TabOrder = 11
  end
  object btnEnPaste: TButton
    Left = 400
    Top = 185
    Width = 75
    Height = 25
    Caption = 'Paste'
    TabOrder = 2
    OnClick = btnEnPasteClick
  end
  object edtCipherText: TEdit
    Left = 8
    Top = 368
    Width = 305
    Height = 21
    Color = 8454143
    TabOrder = 6
    TextHint = 'Cipher text'
  end
  object btnDePaste: TButton
    Left = 400
    Top = 366
    Width = 75
    Height = 25
    Caption = 'Paste'
    TabOrder = 8
    OnClick = btnDePasteClick
  end
  object pnlCipherButtons: TPanel
    Left = 227
    Top = 241
    Width = 185
    Height = 97
    TabOrder = 5
    object lblPasses: TLabel
      Left = 16
      Top = 16
      Width = 37
      Height = 13
      Caption = 'Passes:'
    end
    object btnEncipher: TButton
      Left = 6
      Top = 64
      Width = 75
      Height = 25
      Caption = 'Encipher'
      TabOrder = 1
      OnClick = btnEncipherClick
    end
    object btnDecipher: TButton
      Left = 102
      Top = 64
      Width = 75
      Height = 25
      Caption = 'Decipher'
      TabOrder = 2
      OnClick = btnDecipherClick
    end
    object cboxPasses: TComboBox
      Left = 102
      Top = 12
      Width = 75
      Height = 21
      DropDownCount = 9
      ItemIndex = 0
      MaxLength = 1
      TabOrder = 0
      Text = '1'
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9')
    end
  end
  object btnEnCopy: TButton
    Left = 319
    Top = 185
    Width = 75
    Height = 25
    Caption = 'Copy'
    TabOrder = 1
    OnClick = btnEnCopyClick
  end
  object btnDeCopy: TButton
    Left = 319
    Top = 366
    Width = 75
    Height = 25
    Caption = 'Copy'
    TabOrder = 7
    OnClick = btnDeCopyClick
  end
  object edtPlainText: TEdit
    Left = 8
    Top = 187
    Width = 305
    Height = 21
    Color = 16744576
    TabOrder = 0
    TextHint = 'Plain Text'
  end
  object btnEnImport: TButton
    Left = 481
    Top = 185
    Width = 75
    Height = 25
    Caption = 'Import File...'
    TabOrder = 3
    OnClick = btnEnImportClick
  end
  object btnEnExport: TButton
    Left = 562
    Top = 185
    Width = 75
    Height = 25
    Caption = 'Export File...'
    TabOrder = 4
    OnClick = btnEnExportClick
  end
  object btnDeImport: TButton
    Left = 481
    Top = 366
    Width = 75
    Height = 25
    Caption = 'Import File...'
    TabOrder = 9
    OnClick = btnDeImportClick
  end
  object btnDeExport: TButton
    Left = 562
    Top = 366
    Width = 75
    Height = 25
    Caption = 'Export File...'
    TabOrder = 10
    OnClick = btnDeExportClick
  end
  object OpenTextFileDialog1: TOpenTextFileDialog
    DefaultExt = '.txt'
    Left = 456
    Top = 272
  end
  object SaveTextFileDialog1: TSaveTextFileDialog
    DefaultExt = 'txt'
    FileName = 'Untitled'
    Options = [ofOverwritePrompt, ofCreatePrompt, ofEnableSizing]
    Left = 568
    Top = 272
  end
end
