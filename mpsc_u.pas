// WALDO LEMMER

// Multi-pass mono-alphabetic substitution cipher/decipher program
unit mpsc_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Clipbrd, ExtDlgs;

type
  TfrmMpsc = class(TForm)
    btnEnCopy: TButton;
    btnEnPaste: TButton;
    btnEncipher: TButton;
    btnDecipher: TButton;
    edtCipherText: TEdit;
    btnDeCopy: TButton;
    btnDePaste: TButton;
    progbarProgress: TProgressBar;
    pnlCipherButtons: TPanel;
    cboxPasses: TComboBox;
    lblPasses: TLabel;
    lblInstructions: TLabel;
    edtPlainText: TEdit;
    btnEnImport: TButton;
    btnEnExport: TButton;
    btnDeImport: TButton;
    btnDeExport: TButton;
    OpenTextFileDialog1: TOpenTextFileDialog;
    SaveTextFileDialog1: TSaveTextFileDialog;
    procedure FormCreate(Sender: TObject);
    procedure btnEncipherClick(Sender: TObject);
    procedure btnDecipherClick(Sender: TObject);
    procedure btnEnCopyClick(Sender: TObject);
    procedure btnDeCopyClick(Sender: TObject);
    procedure btnEnPasteClick(Sender: TObject);
    procedure btnDePasteClick(Sender: TObject);
    procedure btnEnImportClick(Sender: TObject);
    procedure btnDeImportClick(Sender: TObject);
    procedure btnEnExportClick(Sender: TObject);
    procedure btnDeExportClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  public
  private
    Key       : array[1..9] of string;
  protected

  end;

var
  frmMpsc: TfrmMpsc;

  Alphabet  : string;

implementation

{$R *.dfm}
 
procedure TfrmMpsc.FormCreate(Sender: TObject);
begin
  // All printable ASCII characters
  Alphabet := ' !"#$%&''()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~';
  // The above characters, but shuffled around with a C++ program I wrote
  Key[1] := '%2J)Hg#.lO~9LXh\["f5qQ0Ax/}w;P F(va4n8^<eSV3M''mC>_YZ-{T$yid1jp,+?sBoEW!|Kzk]R7*uGN6Ic:&b@UtrD=`';
  Key[2] := 'f\r2z^?O+!P*(Be$SM<y`[s)HahI.8RxUX_u>CpJ0/W3G#};olAYNnd"ki t]K-Fg7T@9wZVED~5j%6q4=cv''Q&1|:{mLb,';
  Key[3] := '17E@8?I6R&WiV3mNfy[;Bt9Y^C|#Sx]}!\)`geHuL<4:(-FKzdM$0rDQ%~vO+2T/5lw=aJpn*X''>P"Us_AZco,Gbqh.k{j ';
  Key[4] := 'R;CY5zIvEQ"f@-3dsm4<DV''G=7O*!Bcg[M)U|(>0.qib{jaS^/tNx1$A#L]oeZ8_yJnX\HPTp}k%r&6`~u2w: K?W9h+,lF';
  Key[5] := 'EU4]OWlzce=Y2''|@L7sNk6yRg;)CoT3$+_v`0.<i{!wdqa}#X,\u8b[9(/1>D5H&ArG*K^MQZPF jV"?BtpnxS~:%JmIfh-';
  Key[6] := ' a6nht%,!.B?WZvNp:|d74Mic$''O]Ge9H*)#}uF_@EmCTYPgI3zb[x1-5AJQ08"~j;&USL{f(q/<`>lX^oD+Vw\rR=2sKky';
  Key[7] := '`+~b 2qr;|67^\R[S%hvaxNHZs?t{o*l1gQ}=:CXDzTEJGW@]FM#9,mji_BP!YAKO.e<>L0w38fI4Uu''(&nd$ck-yVp)"/5';
  Key[8] := 'n~Nwu%pxmt0Sb+KUJ?CWQ<['',5d8;!Gi3{"YvIETLr.}^hFy*=-_9PDMq1X(a)kfs$2>4z#7Al`ec:ojgH]Z BV@&/\O|6R';
  Key[9] := '@2\+F&oN,<}XJme8Uh-`C0ypixR;j1=Y{!''7MrS:f[T>cL9.PWAE#GHg$_^?3tkq4B)sb]Ivl*Da(zK/6w~V Q5dO%Z"u|n';
end;

procedure TfrmMpsc.FormShow(Sender: TObject); // FormShow
  begin
    btnEncipher.SetFocus; // I did this because if edtPlainText was focused on form creation, it would not contain the 'Plain Text' hint.
                          // FormShow is used instead of FormCreate because FormCreate is executed before all the components are enabled, causing an error. }
  end;

procedure TfrmMpsc.btnEnCopyClick(Sender: TObject); // Plain text's "Copy" button
  begin
    Clipboard.AsText := edtPlainText.Text;
  end;

procedure TfrmMpsc.btnEnPasteClick(Sender: TObject); // Plain text's "Paste" button
  begin
    edtPlainText.Text := Clipboard.AsText;
  end;

procedure TfrmMpsc.btnEnImportClick(Sender: TObject); // Plain text's "Import File..." button
  var
    tfileContents : TextFile;
    sPlainText    : string;
  begin
    if OpenTextFileDialog1.Execute(Self.Handle) then // Ask the user to choose a text file
      begin
        AssignFile(tfileContents, OpenTextFileDialog1.FileName); // Store a pointer to the file in fileContents
        Reset(tfileContents); // Open the file up for reading/writing
        edtPlainText.Text := ''; // Clear the text box
        
        // Loop through the file until we reach the end
        while not Eof(tfileContents) do
          begin
            // Read one line from the file
            ReadLn(tfileContents, sPlainText);
            edtPlainText.Text := edtPlainText.Text + sPlainText;
          end;
        CloseFile(tfileContents);
      end;
  end;
  
procedure TfrmMpsc.btnEnExportClick(Sender: TObject); // Plain text's "Export File..." button
  var
    tfileContents    : TextFile;
    sPlainText      : string;
  begin
    sPlainText := edtPlainText.Text;

    if SaveTextFileDialog1.Execute(Self.Handle) then // Ask user to choose a location
      begin
        AssignFile(tFileContents, SaveTextFileDialog1.FileName); // Store a pointer to the file location in fileContents
        Rewrite(tFileContents); // Open the file for writing
    
        // Write the string to the file
        if not (sPlainText = '') then
          begin
            WriteLn(tFileContents, sPlainText);
          end;
        CloseFile(tFileContents);
      end;
  end;
  
procedure TfrmMpsc.btnEncipherClick(Sender: TObject); // "Encipher" button
  var
    sPlainText    	: string;
    iPasses       	: integer;
    sCipherText   	: string;

    iCurrentPass  	: integer;
    iCurrentLetter	: integer;
    K            	: integer;
  begin
    sPlainText := edtPlainText.Text;
    iPasses := StrToInt(cboxPasses.Text);

    // Update progress bar
    progbarProgress.Position := 0; // Empty the progress bar
    progbarProgress.Max := iPasses * Length(sPlainText); // Set the correct maximum value. We will add 1 to the position after every pass and letter
    progbarProgress.State := pbsNormal; // Makes the progress bar green, showing that the program is busy

    // Loop through passes
    iCurrentPass := 1 ;
    while iCurrentPass <= iPasses do
      begin
        // Update progress bar
        progbarProgress.Position := progbarProgress.Position + 1;

        sCipherText := '';

        // Loop through letters in plain text
        iCurrentLetter := 1;
        while iCurrentLetter <= Length(sPlainText) do
          begin
            // Update progress bar
            progbarProgress.Position := progbarProgress.Position + 1;

            // Loop through letters in 'alphabet'
            K := 1;
            while K <= Length(Alphabet) do
              begin

                if sPlainText[iCurrentLetter] = Alphabet[K] then // If we've found the letter in Alphabet...
                   begin
                     sCipherText := sCipherText + Key[iCurrentPass][K]; // ...append it to the sCipherText variable

                     // Optimization; ends the loop early because we already found the letter. No need to keep looking for it.
                     // This part wouldn't be possible if I had used a for loop, since Delphi throws an error when modifying a for
                     // loop's index variable.
                     K := Length(Alphabet);
                   end;

                Inc(K);
              end;

            Inc(iCurrentLetter);
          end;

        sPlainText := sCipherText; // sPlainText should now contain the encrypted string. If this isn't the final pass, we will encrypt sPlainText again.
        Inc(iCurrentPass);
      end;

      // Update progress bar
      progbarProgress.State := pbsPaused;

      edtCipherText.Text := sCipherText;
  end;
   
procedure TfrmMpsc.btnDecipherClick(Sender: TObject); // "Decipher" button
  var
    sPlainText    : string;
    iPasses       : integer;
    sCipherText   : string;

    iCurrentPass   : integer;
    iCurrentLetter : integer;
    K             : integer;
  begin
    sCipherText := edtCipherText.Text;
    iPasses := StrToInt(cboxPasses.Text);

    // Update progress bar
    progbarProgress.Position := 0; // Empty the progress bar
    progbarProgress.Max := iPasses * Length(sCipherText); // Set the correct maximum value. We will add 1 to the position after every pass and letter
    progbarProgress.State := pbsNormal; // Makes the progress bar green, showing that the program is busy

    // Loop through passes
    iCurrentPass := iPasses;
    while iCurrentPass >= 1 do
      begin
        // Update progress bar
        progbarProgress.Position := progbarProgress.Position + 1;

        sPlainText := '';

        // Loop through letters in cipher text
        iCurrentLetter := 1;
        while iCurrentLetter <= Length(sCipherText) do
          begin
            // Update progress bar
            progbarProgress.Position := progbarProgress.Position + 1;

            // Loop through letters in key
            K := 1;
            while K <= Length(Key[iCurrentPass]) do
              begin
                if sCipherText[iCurrentLetter] = Key[iCurrentPass][K] then // If we've found the letter in Key[1..6]...
                   begin
                     sPlainText := sPlainText + Alphabet[K]; // ...append it to the sPlainText variable

                     // Optimization; ends the loop early because we already found the letter. No need to keep looking for it.
                     // This part wouldn't be possible if I had used a for loop, since Delphi throws an error when modifying a for
                     // loop's index variable.
                     K := Length(Key[iCurrentPass]);
                   end;

                Inc(K);
              end;

            Inc(iCurrentLetter);
          end;

        sCipherText := sPlainText; // sCipherText should now contain the encrypted string. If this isn't the final pass, we will encrypt sCipherText again.
        Dec(iCurrentPass);
      end;

      // Update progress bar
      progbarProgress.State := pbsPaused;

      edtPlainText.Text := sPlainText;
  end;

procedure TfrmMpsc.btnDeCopyClick(Sender: TObject); // Cipher text's "Copy" button
  begin
    Clipboard.AsText := edtCipherText.Text;
  end;
        
procedure TfrmMpsc.btnDePasteClick(Sender: TObject); // Cipher text's "Paste" button
  begin
    edtCipherText.Text := Clipboard.AsText;
  end;
    
procedure TfrmMpsc.btnDeImportClick(Sender: TObject); // Cipher text's "Import File..." button
  var
    tFileContents  : TextFile;
    sCipherText    : string;
  begin
    if OpenTextFileDialog1.Execute(Self.Handle) then // Ask the user to choose a text file and check if it exists
      begin
        AssignFile(tFileContents, OpenTextFileDialog1.FileName); // Store a pointer to the file in tFileContents
        Reset(tFileContents); // Open the file up for reading/writing
        edtCipherText.Text := ''; // Clear the text box
        
        // Loop through the file until we reach the end
        while not Eof(tFileContents) do
          begin
            // Read one line from the file
            ReadLn(tFileContents, sCipherText);
            edtCipherText.Text := edtCipherText.Text + sCipherText;
          end;
        CloseFile(tFileContents);
      end;
  end;

procedure TfrmMpsc.btnDeExportClick(Sender: TObject); // Cipher text's "Export File..." button
  var
    tFileContents  : TextFile;
    sCipherText   : string;
  begin
    sCipherText := edtPlainText.Text;

    if SaveTextFileDialog1.Execute(Self.Handle) then // Ask user to choose a location
      begin
        AssignFile(tFileContents, SaveTextFileDialog1.FileName); // Store a pointer to the file location in tFileContents
        Rewrite(tFileContents); // Open the file for writing

        // Write the string to the file
        if not (sCipherText = '') then
          begin
            WriteLn(tFileContents, sCipherText);
          end;
        CloseFile(tFileContents);
      end;
  end;

end.
